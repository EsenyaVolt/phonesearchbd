CREATE TABLE [dbo].[provider_Esenya] (
    [Id]    INT           IDENTITY (1, 1) NOT NULL,
    [name]  NVARCHAR (50) NOT NULL,
    [score] FLOAT (53)    NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[abonent_Esenya] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [surname]    NVARCHAR (50) NOT NULL,
    [name]       NVARCHAR (50) NOT NULL,
    [patronymic] NVARCHAR (50) NOT NULL,
    [adrress]    TEXT          NOT NULL,
    [birth_date] DATETIME      NOT NULL,
    [comment]    TEXT          NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[contact_Esenya] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [phone]       NVARCHAR (11) NOT NULL,
    [type]        NVARCHAR (20) NOT NULL,
    [provider_id] INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Esenya_contact-provider] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider_Esenya] ([Id])
);

CREATE TABLE [dbo].[abon_has_cont_Esenya] (
    [contact_id] INT NOT NULL,
    [abonent_id] INT NOT NULL,
    CONSTRAINT [FK_Esenya_abon_has_cont-contact] FOREIGN KEY ([contact_id]) REFERENCES [dbo].[contact_Esenya] ([Id]),
    CONSTRAINT [FK_Esenya_abon_has_cont-abonent] FOREIGN KEY ([abonent_id]) REFERENCES [dbo].[abonent_Esenya] ([Id])
);

