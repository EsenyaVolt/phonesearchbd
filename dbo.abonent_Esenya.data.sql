SET IDENTITY_INSERT [dbo].[abonent_Esenya] ON
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (2, N'Антонова', N'Елена', N'Игоревна', N'НН', N'1996-05-28 00:00:00', N'ОК')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (4, N'Царско', N'Алла', N'Кузьмевна', N'Мск', N'1998-01-21 00:00:00', N'НЕ ОК')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (5, N'Кубацкий', N'Михей', N'Захарович', N'СПб', N'1976-07-23 00:00:00', N'OK')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (6, N'Калашникова', N'Зинаида', N'Илаврентеевна', N'Екб', N'1968-11-11 00:00:00', N'НЕ ОК')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (7, N'Корин', N'Михаил', N'Николаевич', N'Новосибирск', N'1960-09-09 00:00:00', N'ОК')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (8, N'Сомов', N'Алексей', N'Александорович', N'Владивосток', N'1991-10-13 00:00:00', N'НЕ ОК')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (9, N'Зайченко', N'Татьяна', N'Сергеевна', N'Сочи', N'1989-03-30 00:00:00', N'ОК')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (10, N'Яковлев', N'Иван', N'Викторович', N'Сыктывкар', N'1984-09-05 00:00:00', N'НЕ ОК')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (11, N'Данилина', N'Юлия', N'Вячеславовна', N'НН', N'1981-06-06 00:00:00', N'ОК')
INSERT INTO [dbo].[abonent_Esenya] ([Id], [surname], [name], [patronymic], [adrress], [birth_date], [comment]) VALUES (12, N'Прекрасная', N'Василиса', N'Ивановна', N'Сочи', N'1975-10-05 00:00:00', N'НЕ ОК')
SET IDENTITY_INSERT [dbo].[abonent_Esenya] OFF
