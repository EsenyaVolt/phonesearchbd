﻿namespace SQL_BD1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tabControl = new System.Windows.Forms.TabControl();
            this.abonent_tab = new System.Windows.Forms.TabPage();
            this.abonent_dgv = new System.Windows.Forms.DataGridView();
            this.buttonAddAbon = new System.Windows.Forms.Button();
            this.editAbonbutton = new System.Windows.Forms.Button();
            this.buttonDeleteAbon = new System.Windows.Forms.Button();
            this.contact_tab = new System.Windows.Forms.TabPage();
            this.contact_dgv = new System.Windows.Forms.DataGridView();
            this.buttonAddCont = new System.Windows.Forms.Button();
            this.buttonDelCont = new System.Windows.Forms.Button();
            this.buttonEditCont = new System.Windows.Forms.Button();
            this.provider_tab = new System.Windows.Forms.TabPage();
            this.buttonDeleteProvider = new System.Windows.Forms.Button();
            this.provider_dgv = new System.Windows.Forms.DataGridView();
            this.buttonEditProvider = new System.Windows.Forms.Button();
            this.buttonAddProvider = new System.Windows.Forms.Button();
            this.phonesearch_tab = new System.Windows.Forms.TabPage();
            this.add_button = new System.Windows.Forms.Button();
            this.delete_button = new System.Windows.Forms.Button();
            this.edit_button = new System.Windows.Forms.Button();
            this.phonesearch_dgv = new System.Windows.Forms.DataGridView();
            this.help_textBox = new System.Windows.Forms.TextBox();
            this.PhoneNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FIO_textBox = new System.Windows.Forms.TextBox();
            this.main_tabControl.SuspendLayout();
            this.abonent_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.abonent_dgv)).BeginInit();
            this.contact_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contact_dgv)).BeginInit();
            this.provider_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.provider_dgv)).BeginInit();
            this.phonesearch_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phonesearch_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // main_tabControl
            // 
            this.main_tabControl.Controls.Add(this.abonent_tab);
            this.main_tabControl.Controls.Add(this.contact_tab);
            this.main_tabControl.Controls.Add(this.provider_tab);
            this.main_tabControl.Controls.Add(this.phonesearch_tab);
            this.main_tabControl.Location = new System.Drawing.Point(12, 12);
            this.main_tabControl.Name = "main_tabControl";
            this.main_tabControl.SelectedIndex = 0;
            this.main_tabControl.Size = new System.Drawing.Size(794, 428);
            this.main_tabControl.TabIndex = 1;
            // 
            // abonent_tab
            // 
            this.abonent_tab.Controls.Add(this.abonent_dgv);
            this.abonent_tab.Controls.Add(this.buttonAddAbon);
            this.abonent_tab.Controls.Add(this.editAbonbutton);
            this.abonent_tab.Controls.Add(this.buttonDeleteAbon);
            this.abonent_tab.Location = new System.Drawing.Point(4, 22);
            this.abonent_tab.Name = "abonent_tab";
            this.abonent_tab.Padding = new System.Windows.Forms.Padding(3);
            this.abonent_tab.Size = new System.Drawing.Size(786, 402);
            this.abonent_tab.TabIndex = 0;
            this.abonent_tab.Text = "Абоненты";
            this.abonent_tab.UseVisualStyleBackColor = true;
            // 
            // abonent_dgv
            // 
            this.abonent_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.abonent_dgv.Location = new System.Drawing.Point(3, 3);
            this.abonent_dgv.MultiSelect = false;
            this.abonent_dgv.Name = "abonent_dgv";
            this.abonent_dgv.Size = new System.Drawing.Size(563, 367);
            this.abonent_dgv.TabIndex = 0;
            // 
            // buttonAddAbon
            // 
            this.buttonAddAbon.Location = new System.Drawing.Point(6, 376);
            this.buttonAddAbon.Name = "buttonAddAbon";
            this.buttonAddAbon.Size = new System.Drawing.Size(130, 24);
            this.buttonAddAbon.TabIndex = 6;
            this.buttonAddAbon.Text = "Добавить абонента";
            this.buttonAddAbon.UseVisualStyleBackColor = true;
            this.buttonAddAbon.Click += new System.EventHandler(this.buttonAddAbon_Click);
            // 
            // editAbonbutton
            // 
            this.editAbonbutton.Location = new System.Drawing.Point(142, 376);
            this.editAbonbutton.Name = "editAbonbutton";
            this.editAbonbutton.Size = new System.Drawing.Size(142, 24);
            this.editAbonbutton.TabIndex = 7;
            this.editAbonbutton.Text = "Редактировать абонента";
            this.editAbonbutton.UseVisualStyleBackColor = true;
            this.editAbonbutton.Click += new System.EventHandler(this.editAbonbutton_Click);
            // 
            // buttonDeleteAbon
            // 
            this.buttonDeleteAbon.Location = new System.Drawing.Point(290, 376);
            this.buttonDeleteAbon.Name = "buttonDeleteAbon";
            this.buttonDeleteAbon.Size = new System.Drawing.Size(112, 24);
            this.buttonDeleteAbon.TabIndex = 8;
            this.buttonDeleteAbon.Text = "Удалить абонента";
            this.buttonDeleteAbon.UseVisualStyleBackColor = true;
            this.buttonDeleteAbon.Click += new System.EventHandler(this.buttonDeleteAbon_Click);
            // 
            // contact_tab
            // 
            this.contact_tab.Controls.Add(this.contact_dgv);
            this.contact_tab.Controls.Add(this.buttonAddCont);
            this.contact_tab.Controls.Add(this.buttonDelCont);
            this.contact_tab.Controls.Add(this.buttonEditCont);
            this.contact_tab.Location = new System.Drawing.Point(4, 22);
            this.contact_tab.Name = "contact_tab";
            this.contact_tab.Padding = new System.Windows.Forms.Padding(3);
            this.contact_tab.Size = new System.Drawing.Size(786, 402);
            this.contact_tab.TabIndex = 1;
            this.contact_tab.Text = "Контакты";
            this.contact_tab.UseVisualStyleBackColor = true;
            // 
            // contact_dgv
            // 
            this.contact_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.contact_dgv.Location = new System.Drawing.Point(3, 3);
            this.contact_dgv.MultiSelect = false;
            this.contact_dgv.Name = "contact_dgv";
            this.contact_dgv.Size = new System.Drawing.Size(368, 363);
            this.contact_dgv.TabIndex = 0;
            // 
            // buttonAddCont
            // 
            this.buttonAddCont.Location = new System.Drawing.Point(6, 372);
            this.buttonAddCont.Name = "buttonAddCont";
            this.buttonAddCont.Size = new System.Drawing.Size(130, 24);
            this.buttonAddCont.TabIndex = 9;
            this.buttonAddCont.Text = "Добавить контакт";
            this.buttonAddCont.UseVisualStyleBackColor = true;
            this.buttonAddCont.Click += new System.EventHandler(this.buttonAddCont_Click);
            // 
            // buttonDelCont
            // 
            this.buttonDelCont.Location = new System.Drawing.Point(290, 372);
            this.buttonDelCont.Name = "buttonDelCont";
            this.buttonDelCont.Size = new System.Drawing.Size(112, 24);
            this.buttonDelCont.TabIndex = 11;
            this.buttonDelCont.Text = "Удалить контакт";
            this.buttonDelCont.UseVisualStyleBackColor = true;
            this.buttonDelCont.Click += new System.EventHandler(this.buttonDelCont_Click);
            // 
            // buttonEditCont
            // 
            this.buttonEditCont.Location = new System.Drawing.Point(142, 372);
            this.buttonEditCont.Name = "buttonEditCont";
            this.buttonEditCont.Size = new System.Drawing.Size(142, 24);
            this.buttonEditCont.TabIndex = 10;
            this.buttonEditCont.Text = "Редактировать контакт";
            this.buttonEditCont.UseVisualStyleBackColor = true;
            this.buttonEditCont.Click += new System.EventHandler(this.buttonEditCont_Click);
            // 
            // provider_tab
            // 
            this.provider_tab.Controls.Add(this.buttonDeleteProvider);
            this.provider_tab.Controls.Add(this.provider_dgv);
            this.provider_tab.Controls.Add(this.buttonEditProvider);
            this.provider_tab.Controls.Add(this.buttonAddProvider);
            this.provider_tab.Location = new System.Drawing.Point(4, 22);
            this.provider_tab.Name = "provider_tab";
            this.provider_tab.Padding = new System.Windows.Forms.Padding(3);
            this.provider_tab.Size = new System.Drawing.Size(786, 402);
            this.provider_tab.TabIndex = 2;
            this.provider_tab.Text = "Провайдер";
            this.provider_tab.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteProvider
            // 
            this.buttonDeleteProvider.Location = new System.Drawing.Point(311, 375);
            this.buttonDeleteProvider.Name = "buttonDeleteProvider";
            this.buttonDeleteProvider.Size = new System.Drawing.Size(142, 24);
            this.buttonDeleteProvider.TabIndex = 14;
            this.buttonDeleteProvider.Text = "Удалить провайдера";
            this.buttonDeleteProvider.UseVisualStyleBackColor = true;
            this.buttonDeleteProvider.Click += new System.EventHandler(this.buttonDeleteProvider_Click);
            // 
            // provider_dgv
            // 
            this.provider_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.provider_dgv.Location = new System.Drawing.Point(3, 3);
            this.provider_dgv.MultiSelect = false;
            this.provider_dgv.Name = "provider_dgv";
            this.provider_dgv.Size = new System.Drawing.Size(248, 366);
            this.provider_dgv.TabIndex = 0;
            // 
            // buttonEditProvider
            // 
            this.buttonEditProvider.Location = new System.Drawing.Point(139, 375);
            this.buttonEditProvider.Name = "buttonEditProvider";
            this.buttonEditProvider.Size = new System.Drawing.Size(166, 24);
            this.buttonEditProvider.TabIndex = 13;
            this.buttonEditProvider.Text = "Редактировать провайдера";
            this.buttonEditProvider.UseVisualStyleBackColor = true;
            this.buttonEditProvider.Click += new System.EventHandler(this.buttonEditProvider_Click);
            // 
            // buttonAddProvider
            // 
            this.buttonAddProvider.Location = new System.Drawing.Point(3, 375);
            this.buttonAddProvider.Name = "buttonAddProvider";
            this.buttonAddProvider.Size = new System.Drawing.Size(130, 24);
            this.buttonAddProvider.TabIndex = 12;
            this.buttonAddProvider.Text = "Добавить провайдера";
            this.buttonAddProvider.UseVisualStyleBackColor = true;
            this.buttonAddProvider.Click += new System.EventHandler(this.buttonAddProvider_Click);
            // 
            // phonesearch_tab
            // 
            this.phonesearch_tab.Controls.Add(this.add_button);
            this.phonesearch_tab.Controls.Add(this.delete_button);
            this.phonesearch_tab.Controls.Add(this.edit_button);
            this.phonesearch_tab.Controls.Add(this.phonesearch_dgv);
            this.phonesearch_tab.Location = new System.Drawing.Point(4, 22);
            this.phonesearch_tab.Name = "phonesearch_tab";
            this.phonesearch_tab.Padding = new System.Windows.Forms.Padding(3);
            this.phonesearch_tab.Size = new System.Drawing.Size(786, 402);
            this.phonesearch_tab.TabIndex = 3;
            this.phonesearch_tab.Text = "Телефонный справочник";
            this.phonesearch_tab.UseVisualStyleBackColor = true;
            // 
            // add_button
            // 
            this.add_button.Location = new System.Drawing.Point(5, 372);
            this.add_button.Name = "add_button";
            this.add_button.Size = new System.Drawing.Size(76, 24);
            this.add_button.TabIndex = 12;
            this.add_button.Text = "Добавить";
            this.add_button.UseVisualStyleBackColor = true;
            this.add_button.Click += new System.EventHandler(this.add_button_Click);
            // 
            // delete_button
            // 
            this.delete_button.Location = new System.Drawing.Point(188, 372);
            this.delete_button.Name = "delete_button";
            this.delete_button.Size = new System.Drawing.Size(65, 24);
            this.delete_button.TabIndex = 14;
            this.delete_button.Text = "Удалить";
            this.delete_button.UseVisualStyleBackColor = true;
            this.delete_button.Click += new System.EventHandler(this.delete_button_Click);
            // 
            // edit_button
            // 
            this.edit_button.Location = new System.Drawing.Point(87, 372);
            this.edit_button.Name = "edit_button";
            this.edit_button.Size = new System.Drawing.Size(95, 24);
            this.edit_button.TabIndex = 13;
            this.edit_button.Text = "Редактировать";
            this.edit_button.UseVisualStyleBackColor = true;
            this.edit_button.Click += new System.EventHandler(this.edit_button_Click);
            // 
            // phonesearch_dgv
            // 
            this.phonesearch_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.phonesearch_dgv.Location = new System.Drawing.Point(3, 3);
            this.phonesearch_dgv.Name = "phonesearch_dgv";
            this.phonesearch_dgv.Size = new System.Drawing.Size(765, 365);
            this.phonesearch_dgv.TabIndex = 0;
            // 
            // help_textBox
            // 
            this.help_textBox.Location = new System.Drawing.Point(12, 446);
            this.help_textBox.Name = "help_textBox";
            this.help_textBox.Size = new System.Drawing.Size(161, 20);
            this.help_textBox.TabIndex = 2;
            this.help_textBox.TextChanged += new System.EventHandler(this.help_textBox_TextChanged);
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.AutoSize = true;
            this.PhoneNumber.Location = new System.Drawing.Point(186, 450);
            this.PhoneNumber.Name = "PhoneNumber";
            this.PhoneNumber.Size = new System.Drawing.Size(93, 13);
            this.PhoneNumber.TabIndex = 3;
            this.PhoneNumber.Text = "Номер телефона";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(459, 450);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "ФИО";
            // 
            // FIO_textBox
            // 
            this.FIO_textBox.Location = new System.Drawing.Point(285, 446);
            this.FIO_textBox.Name = "FIO_textBox";
            this.FIO_textBox.Size = new System.Drawing.Size(161, 20);
            this.FIO_textBox.TabIndex = 4;
            this.FIO_textBox.TextChanged += new System.EventHandler(this.FIO_textBox_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 478);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FIO_textBox);
            this.Controls.Add(this.PhoneNumber);
            this.Controls.Add(this.help_textBox);
            this.Controls.Add(this.main_tabControl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.main_tabControl.ResumeLayout(false);
            this.abonent_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.abonent_dgv)).EndInit();
            this.contact_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.contact_dgv)).EndInit();
            this.provider_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.provider_dgv)).EndInit();
            this.phonesearch_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.phonesearch_dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl main_tabControl;
        private System.Windows.Forms.TabPage abonent_tab;
        private System.Windows.Forms.TabPage contact_tab;
        private System.Windows.Forms.TabPage provider_tab;
        private System.Windows.Forms.TabPage phonesearch_tab;
        private System.Windows.Forms.DataGridView phonesearch_dgv;
        private System.Windows.Forms.DataGridView abonent_dgv;
        private System.Windows.Forms.DataGridView contact_dgv;
        private System.Windows.Forms.DataGridView provider_dgv;
        private System.Windows.Forms.TextBox help_textBox;
        private System.Windows.Forms.Label PhoneNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FIO_textBox;
        private System.Windows.Forms.Button buttonAddAbon;
        public System.Windows.Forms.Button editAbonbutton;
        public System.Windows.Forms.Button buttonDeleteAbon;
        public System.Windows.Forms.Button buttonDelCont;
        public System.Windows.Forms.Button buttonEditCont;
        private System.Windows.Forms.Button buttonAddCont;
        public System.Windows.Forms.Button buttonDeleteProvider;
        public System.Windows.Forms.Button buttonEditProvider;
        private System.Windows.Forms.Button buttonAddProvider;
        private System.Windows.Forms.Button add_button;
        public System.Windows.Forms.Button delete_button;
        public System.Windows.Forms.Button edit_button;
    }
}

