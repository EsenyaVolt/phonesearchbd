﻿namespace SQL_BD1
{
    partial class AbonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.surname_textBox = new System.Windows.Forms.TextBox();
            this.name_textBox = new System.Windows.Forms.TextBox();
            this.patronymic_textBox = new System.Windows.Forms.TextBox();
            this.adrress_textBox = new System.Windows.Forms.TextBox();
            this.comment_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.OK_button = new System.Windows.Forms.Button();
            this.otmena_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // surname_textBox
            // 
            this.surname_textBox.Location = new System.Drawing.Point(12, 12);
            this.surname_textBox.Name = "surname_textBox";
            this.surname_textBox.Size = new System.Drawing.Size(105, 20);
            this.surname_textBox.TabIndex = 0;
            // 
            // name_textBox
            // 
            this.name_textBox.Location = new System.Drawing.Point(12, 38);
            this.name_textBox.Name = "name_textBox";
            this.name_textBox.Size = new System.Drawing.Size(105, 20);
            this.name_textBox.TabIndex = 1;
            // 
            // patronymic_textBox
            // 
            this.patronymic_textBox.Location = new System.Drawing.Point(12, 64);
            this.patronymic_textBox.Name = "patronymic_textBox";
            this.patronymic_textBox.Size = new System.Drawing.Size(105, 20);
            this.patronymic_textBox.TabIndex = 2;
            // 
            // adrress_textBox
            // 
            this.adrress_textBox.Location = new System.Drawing.Point(12, 90);
            this.adrress_textBox.Name = "adrress_textBox";
            this.adrress_textBox.Size = new System.Drawing.Size(105, 20);
            this.adrress_textBox.TabIndex = 3;
            // 
            // comment_textBox
            // 
            this.comment_textBox.Location = new System.Drawing.Point(12, 116);
            this.comment_textBox.Name = "comment_textBox";
            this.comment_textBox.Size = new System.Drawing.Size(105, 20);
            this.comment_textBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(123, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Фамилия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(123, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(123, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Отчество";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(123, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Адрес";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(123, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Комментарий";
            // 
            // OK_button
            // 
            this.OK_button.Location = new System.Drawing.Point(12, 142);
            this.OK_button.Name = "OK_button";
            this.OK_button.Size = new System.Drawing.Size(80, 24);
            this.OK_button.TabIndex = 10;
            this.OK_button.Text = "OK";
            this.OK_button.UseVisualStyleBackColor = true;
            this.OK_button.Click += new System.EventHandler(this.OK_button_Click);
            // 
            // otmena_button
            // 
            this.otmena_button.Location = new System.Drawing.Point(120, 142);
            this.otmena_button.Name = "otmena_button";
            this.otmena_button.Size = new System.Drawing.Size(80, 24);
            this.otmena_button.TabIndex = 11;
            this.otmena_button.Text = "Отмена";
            this.otmena_button.UseVisualStyleBackColor = true;
            this.otmena_button.Click += new System.EventHandler(this.otmena_button_Click);
            // 
            // AbonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(212, 171);
            this.Controls.Add(this.otmena_button);
            this.Controls.Add(this.OK_button);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comment_textBox);
            this.Controls.Add(this.adrress_textBox);
            this.Controls.Add(this.patronymic_textBox);
            this.Controls.Add(this.name_textBox);
            this.Controls.Add(this.surname_textBox);
            this.Name = "AbonForm";
            this.Text = "Abonent";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button OK_button;
        private System.Windows.Forms.Button otmena_button;
        public System.Windows.Forms.TextBox surname_textBox;
        public System.Windows.Forms.TextBox name_textBox;
        public System.Windows.Forms.TextBox patronymic_textBox;
        public System.Windows.Forms.TextBox adrress_textBox;
        public System.Windows.Forms.TextBox comment_textBox;
    }
}