﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_BD1
{
    public partial class FormContact : Form
    {
        public FormContact()
        {
            InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public Dictionary <int, string> ProviderData
        {
            set 
            {
                provider_comboBox.DataSource = value.ToArray();
                provider_comboBox.DisplayMember = "Value";
            }
        }

        public int ProviderId
        {
            get
            {
                return ((KeyValuePair<int, string>)provider_comboBox.SelectedItem).Key; ;
            }

            set
            {
                int idx = 0;
                foreach(KeyValuePair<int,string> item in provider_comboBox.Items)
                {
                    if(item.Key == value)
                    {
                        break;
                    }
                    idx++;
                }
                provider_comboBox.SelectedIndex = idx;
            }

        }
    }
}
