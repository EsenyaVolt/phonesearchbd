﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_BD1
{
    public partial class FormSpravka : Form
    {
        public FormSpravka()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonCANCEL_Click_1(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public Dictionary<int, string> AbonentData
        {
            set
            {
                Abon_comboBox.DataSource = value.ToArray();
                Abon_comboBox.DisplayMember = "Value";
            }
        }

        public int AbonentId
        {
            get
            {
                return ((KeyValuePair<int, string>) Abon_comboBox.SelectedItem).Key; ;
            }

            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in Abon_comboBox.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    idx++;
                }
                Abon_comboBox.SelectedIndex = idx;
            }

        }

        public Dictionary<int, string> ContactData
        {
            set
            {
                Cont_comboBox.DataSource = value.ToArray();
                Cont_comboBox.DisplayMember = "Value";
            }
        }

        public int ContactId
        {
            get
            {
                return ((KeyValuePair<int, string>)Cont_comboBox.SelectedItem).Key; ;
            }

            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in Cont_comboBox.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    idx++;
                }
                Cont_comboBox.SelectedIndex = idx;
            }

        }
    }
}
