﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SQL_BD1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private const string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Елена\\Desktop\\SQL_BD1\\SQL_BD1\\Database1.mdf;Integrated Security=True";
       
        void updateAbonentDGV()
        {
            var request = "SELECT * FROM abonent_Esenya";
            var adapter = new SqlDataAdapter(request, connectionString);
            DataTable abonentTable = new DataTable();
            adapter.Fill(abonentTable);
            abonent_dgv.DataSource = abonentTable;

            abonent_dgv.Columns["id"].Visible = false;
            abonent_dgv.Columns["birth_date"].Visible = false;
            abonent_dgv.Columns["surname"].HeaderText = "Фамилия";
            abonent_dgv.Columns["name"].HeaderText = "Имя";
            abonent_dgv.Columns["patronymic"].HeaderText = "Отчество";
            abonent_dgv.Columns["adrress"].HeaderText = "Адрес";
            abonent_dgv.Columns["comment"].HeaderText = "Комментарии";
        }

        void updateContactDGV()
        {
            var request = "SELECT * FROM contact_Esenya";
            var adapter = new SqlDataAdapter(request, connectionString);
            DataTable contactTable = new DataTable();
            adapter.Fill(contactTable);
            contact_dgv.DataSource = contactTable;

            contact_dgv.Columns["id"].Visible = false;
            contact_dgv.Columns["phone"].HeaderText = "Телефон";
            contact_dgv.Columns["type"].HeaderText = "Тип телефона";
            contact_dgv.Columns["provider_id"].HeaderText = "Провайдер id";
        }

        void updateProviderDGV()
        {
            var request = "SELECT * FROM provider_Esenya";
            var adapter = new SqlDataAdapter(request, connectionString);
            DataTable providerTable = new DataTable();
            adapter.Fill(providerTable);
            provider_dgv.DataSource = providerTable;

            provider_dgv.Columns["id"].Visible = false;
            provider_dgv.Columns["name"].HeaderText = "Провайдер";
            provider_dgv.Columns["score"].HeaderText = "Рейтинг";
        }

        void updatePhoneSearchDGV()
        {
            var request = @"SELECT * FROM abonent_Esenya JOIN abon_has_cont_Esenya
                                ON abonent_Esenya.Id=abon_has_cont_Esenya.abonent_id
                            JOIN contact_Esenya
                                ON contact_Esenya.Id=abon_has_cont_Esenya.contact_id
                                LEFT JOIN provider_Esenya
                                ON provider_Esenya.Id=contact_Esenya.provider_id
                                ";

            if(help_textBox.Text!="")
            {
                request += @"WHERE contact_Esenya.phone LIKE '%" + help_textBox.Text + "%'";
            }

            if (FIO_textBox.Text != "")
            {
                request += @"WHERE abonent_Esenya.surname+' '+abonent_Esenya.name+' '+abonent_Esenya.patronymic 
								LIKE '%" + FIO_textBox.Text + "%'";
            }

            var adapter = new SqlDataAdapter(request, connectionString);
            var SearchTable = new DataTable();
            adapter.Fill(SearchTable);
            phonesearch_dgv.DataSource = SearchTable;

            phonesearch_dgv.Columns["id"].Visible = false;
            phonesearch_dgv.Columns["contact_id"].Visible = false;
            phonesearch_dgv.Columns["birth_date"].Visible = false;
            phonesearch_dgv.Columns["comment"].Visible = false;
            phonesearch_dgv.Columns["abonent_id"].Visible = false;
            phonesearch_dgv.Columns["Id1"].Visible = false;
            phonesearch_dgv.Columns["Id2"].Visible = false;
            phonesearch_dgv.Columns["score"].Visible = false;
            phonesearch_dgv.Columns["provider_id"].Visible = false;

            phonesearch_dgv.Columns["surname"].HeaderText = "Фамилия";
            phonesearch_dgv.Columns["name"].HeaderText = "Имя";
            phonesearch_dgv.Columns["patronymic"].HeaderText = "Отчество";
            phonesearch_dgv.Columns["name1"].HeaderText = "Провайдер";
            phonesearch_dgv.Columns["type"].HeaderText = "Тип телефона";
            phonesearch_dgv.Columns["phone"].HeaderText = "Телефон";
            phonesearch_dgv.Columns["adrress"].HeaderText = "Адрес";
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
            updateAbonentDGV();
            updateContactDGV();
            updateProviderDGV();
            updatePhoneSearchDGV();
        }

        private void help_textBox_TextChanged(object sender, EventArgs e)
        {
            updatePhoneSearchDGV();
        }

        private void FIO_textBox_TextChanged(object sender, EventArgs e)
        {
            updatePhoneSearchDGV();
        }

        private void buttonAddAbon_Click(object sender, EventArgs e)
        {
            var form = new AbonForm();
            var rez = form.ShowDialog();
            if(rez == DialogResult.OK)
            {
                var surname = form.surname_textBox.Text;
                var name = form.name_textBox.Text;
                var patronymic = form.patronymic_textBox.Text;
                var comment = form.comment_textBox.Text;
                var adrress = form.adrress_textBox.Text;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = "INSERT INTO abonent_Esenya(name, surname, patronymic,comment,adrress) " +
                    "VALUES('" + surname + "', '" + name + "', '" + patronymic + "', '" + comment + "', '" + adrress + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGV();
            }
        }

        private void editAbonbutton_Click(object sender, EventArgs e)
        {
            var row = abonent_dgv.SelectedRows.Count > 0 ? abonent_dgv.SelectedRows[0] : null;
            if (row==null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new AbonForm();
            form.surname_textBox.Text = row.Cells["surname"].Value.ToString();
            form.name_textBox.Text = row.Cells["name"].Value.ToString();
            form.patronymic_textBox.Text = row.Cells["patronymic"].Value.ToString();
            form.comment_textBox.Text = row.Cells["comment"].Value.ToString();
            form.adrress_textBox.Text = row.Cells["adrress"].Value.ToString();
            var rez = form.ShowDialog();
            if (rez == DialogResult.OK)
            {
                var surname = form.surname_textBox.Text;
                var name = form.name_textBox.Text;
                var patronymic = form.patronymic_textBox.Text;
                var comment = form.comment_textBox.Text;
                var adrress = form.adrress_textBox.Text;
                var id = row.Cells["id"].Value.ToString();
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = "UPDATE abonent_Esenya SET surname='" + surname + "', name='" + name + "', patronymic='" + patronymic + "', comment='" + comment + "', adrress='" + adrress + "'WHERE id=" + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGV();
            }
        }

        private void buttonDeleteAbon_Click(object sender, EventArgs e)
        {
            var row = abonent_dgv.SelectedRows.Count > 0 ? abonent_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var id = row.Cells["id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = "DELETE FROM abonent_Esenya WHERE id=" + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateAbonentDGV();
        }

        private void buttonAddCont_Click(object sender, EventArgs e)
        {
            var form = new FormContact();
            {
                var request = "SELECT * FROM provider_Esenya";
                var adapter = new SqlDataAdapter(request, connectionString);
                var providTable = new DataTable();
                adapter.Fill(providTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in providTable.Rows)
                {
                    dict.Add((int)row["id"], row["name"].ToString());
                }
                form.ProviderData = dict;
            }
            var rez = form.ShowDialog();
            

            if (rez == DialogResult.OK)
            {
                var phone = form.phone_textBox.Text;
                var type = form.type_textBox.Text;
                var id_provider = form.ProviderId;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = "INSERT INTO contact_Esenya(phone, type, provider_id) " +
                    "VALUES('" + phone + "', '" + type + "', '" + id_provider.ToString() + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateContactDGV();
            }
        }

        private void buttonEditCont_Click(object sender, EventArgs e)
        {
            var row = contact_dgv.SelectedRows.Count > 0 ? contact_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new FormContact();
            form.phone_textBox.Text = row.Cells["phone"].Value.ToString();
            form.type_textBox.Text = row.Cells["type"].Value.ToString();
            {
                var request = "SELECT * FROM provider_Esenya";
                var adapter = new SqlDataAdapter(request, connectionString);
                var providTable = new DataTable();
                adapter.Fill(providTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow dbrow in providTable.Rows)
                {
                    dict.Add((int)dbrow["id"], dbrow["name"].ToString());
                }
                form.ProviderData = dict;
            }
            form.ProviderId = (int)row.Cells["provider_id"].Value;
            var rez = form.ShowDialog();
            if (rez == DialogResult.OK)
            {
                var phone = form.phone_textBox.Text;
                var type = form.type_textBox.Text;
                var id_provider = form.ProviderId;
                var id = row.Cells["id"].Value.ToString();
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = "UPDATE contact_Esenya SET phone='" + phone + "', type='" + type + "', provider_id='" + id_provider.ToString() + "'WHERE id=" + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateContactDGV();
            }
        }

        private void buttonDelCont_Click(object sender, EventArgs e)
        {
            var row = contact_dgv.SelectedRows.Count > 0 ? contact_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var id = row.Cells["id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = "DELETE FROM contact_Esenya WHERE id=" + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateContactDGV();
        }

        private void buttonAddProvider_Click(object sender, EventArgs e)
        {
            var form = new FormProvider();
            var rez = form.ShowDialog();
            if (rez == DialogResult.OK)
            {
                var name = form.name_textBox.Text;
                var score = form.score_textBox.Text;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = "INSERT INTO provider_Esenya(name, score) " +
                    "VALUES('" + name + "', '" + score + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateProviderDGV();
            }
        }

        private void buttonEditProvider_Click(object sender, EventArgs e)
        {
            var row = provider_dgv.SelectedRows.Count > 0 ? provider_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new FormProvider();
            form.name_textBox.Text = row.Cells["name"].Value.ToString();
            form.score_textBox.Text = row.Cells["score"].Value.ToString();
            var rez = form.ShowDialog();
            if (rez == DialogResult.OK)
            {
                var name = form.name_textBox.Text;
                var score = form.score_textBox.Text;
                var id = row.Cells["id"].Value.ToString();
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = "UPDATE provider_Esenya SET name='" + name + "', score='" + score + "'WHERE id=" + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateProviderDGV();
            }
        }

        private void buttonDeleteProvider_Click(object sender, EventArgs e)
        {
            var row = provider_dgv.SelectedRows.Count > 0 ? provider_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var id = row.Cells["id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = "DELETE FROM provider_Esenya WHERE id=" + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateProviderDGV();
            updateContactDGV();
        }

        private void add_button_Click(object sender, EventArgs e)
        {
            var form = new FormSpravka();
            {
                var request = "SELECT id, surname + ' ' + name + ' ' + patronymic AS fio FROM abonent_Esenya";
                var adapter = new SqlDataAdapter(request, connectionString);
                var abonTable = new DataTable();
                adapter.Fill(abonTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in abonTable.Rows)
                {
                    dict.Add((int)row["id"], row["fio"].ToString());
                }
                form.AbonentData = dict;
            }

            {
                var request = "SELECT id, phone + ' (' + type + ' )'  AS cont FROM contact_Esenya";
                var adapter = new SqlDataAdapter(request, connectionString);
                var contTable = new DataTable();
                adapter.Fill(contTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in contTable.Rows)
                {
                    dict.Add((int)row["id"], row["cont"].ToString());
                }
                form.ContactData = dict;
            }
            var rez = form.ShowDialog();

            if (rez == DialogResult.OK)
            {
                var abonent = form.AbonentId;
                var contact = form.ContactId;

                var connection = new SqlConnection(connectionString);
                connection.Open();

                var request = @"INSERT INTO abon_has_cont_Esenya(contact_id, abonent_id) " +
                    "VALUES('" + contact.ToString() + "', '" + abonent.ToString() + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updatePhoneSearchDGV();
            }
        }

        private void delete_button_Click(object sender, EventArgs e)
        {
            var row = phonesearch_dgv.SelectedRows.Count > 0 ? phonesearch_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var contact = (int)row.Cells["contact_id"].Value;
            var abonent = (int)row.Cells["abonent_id"].Value;
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = "DELETE FROM abon_has_cont_Esenya WHERE contact_id=" + contact.ToString() + "AND abonent_id=" + abonent.ToString() + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updatePhoneSearchDGV();
        }

        private void edit_button_Click(object sender, EventArgs e)
        {
            var row = phonesearch_dgv.SelectedRows.Count > 0 ? phonesearch_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new FormSpravka();
            {
                var request = "SELECT id, surname + ' ' + name + ' ' + patronymic AS fio FROM abonent_Esenya";
                var adapter = new SqlDataAdapter(request, connectionString);
                var abonTable = new DataTable();
                adapter.Fill(abonTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow dbrow in abonTable.Rows)
                {
                    dict.Add((int)dbrow["id"], dbrow["fio"].ToString());
                }
                form.AbonentData = dict;
            }

            {
                var request = "SELECT id, phone + ' (' + type + ' )'  AS cont FROM contact_Esenya";
                var adapter = new SqlDataAdapter(request, connectionString);
                var contTable = new DataTable();
                adapter.Fill(contTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow dbrow in contTable.Rows)
                {
                    dict.Add((int)dbrow["id"], dbrow["cont"].ToString());
                }
                form.ContactData = dict;
            }

            form.AbonentId = (int)row.Cells["abonent_id"].Value;
            form.ContactId = (int)row.Cells["contact_id"].Value;

            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var abonent = form.AbonentId;
                var contact = form.ContactId;
                var abonentid = row.Cells["abonent_id"].Value.ToString();
                var contactid = row.Cells["contact_id"].Value.ToString();

                var connection = new SqlConnection(connectionString);
                connection.Open();

                var request = @"UPDATE abon_has_cont_Esenya SET 
                        contact_id='" + contact.ToString() + "', abonent_id='" + abonent.ToString() + "' " +
                        "WHERE contact_id=" + contactid.ToString() + " AND abonent_id=" + abonentid.ToString() + "";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();

                connection.Close();
                updatePhoneSearchDGV();
            }
        }
    }
}
